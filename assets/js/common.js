$(document).ready(function(){
	
	$('.scroll_1_bg').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		swipe: false,
		autoplay: true,
		autoplaySpeed: 3000,
		dots: true
	});

	$('.testemonials_slide').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow:'<span class="arrow arrowL"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
		nextArrow:'<span class="arrow arrowR"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
		responsive: [
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}
		]
	});

	$('.slick_depsre_certmatcon').slick({
		infinite: false,
		slidesToShow: 3,
		slidesToScroll: 1,
		dots: true,
		useTransform: true,
		initialSlide: 1,
		centerMode: true
	});

	$('.scrl_logo_slider').slick({
		infinite: false,
		slidesToShow: 5,
		slidesToScroll: 1,
		prevArrow:'<span class="arrow arrowL"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
		nextArrow:'<span class="arrow arrowR"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
		responsive: [
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 4,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1
			}
		},
		{
			breakpoint: 480,
			settings: {
				slidesToShow: 2,
				slidesToScroll: 1
			}
		}
		]
	});

	$('#menu_id').load('/menu.html');
	$('#footer_id').load('/footer.html');

	

})